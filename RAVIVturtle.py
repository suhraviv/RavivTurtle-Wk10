#Raviv Haeems
#11/11/2016
#Turtle Shape Drawer

import turtle


def Square(color, size): #square shape
    turtle.Screen()
    turtle.color(color) #uses color that user inputs
    turtle.forward(size) #uses size that user inputs
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.done()

def Triangle(color, size): #triangle shaple
    turtle.Screen()
    turtle.color(color) #uses color that user inputs
    turtle.forward(size) #uses size that user inputs
    turtle.right(120)
    turtle.forward(size)
    turtle.right(120)
    turtle.forward(size)
    turtle.right(120)
    turtle.done()

def Star(color, size): #star shape
    turtle.Screen()
    turtle.color(color) #uses color that user inputs
    turtle.forward(size) #uses size that user inputs
    turtle.right(144)
    turtle.forward(size)
    turtle.right(144)
    turtle.forward(size)
    turtle.right(144)
    turtle.forward(size)
    turtle.right(144)
    turtle.forward(size)
    turtle.right(144)
    turtle.done()



def Hexagon(color, size): #hexagon shape
    turtle.Screen()
    turtle.color(color) #uses color that user inputs
    turtle.forward(size) #uses size that user inputs
    turtle.right(60)
    turtle.forward(size)
    turtle.right(60)
    turtle.forward(size)
    turtle.right(60)
    turtle.forward(size)
    turtle.right(60)
    turtle.forward(size)
    turtle.right(60)
    turtle.forward(size)
    turtle.right(60)
    turtle.forward(size)
    turtle.done()

def Circle(color, size): #circle shape
        turtle.color(color) #uses color that user inputs
        turtle.circle(size) #uses size that user inputs
        turtle.done()

print("Hello. I am the geometry guy dude.\nI can construct a Hexagon, a Star, a Triangle, a Circle, or a Square")
print("What should I draw?")
shape=input()

#what color?
print("what color do you want the {} to be? ".format(shape))

shapecolor = input()

#what size?
print("What size:\nSmall, Medium, or Large?")
sizeinput = input()
shape = shape.lower()

if sizeinput == "small": #if user inputs small
    size = 40

if sizeinput == "medium": #if user inputs medium
    size = 80

if sizeinput == "large": #if user inputs large
    size = 100


if shape == "square": #if user inputs square
    Square(shapecolor, size)

if shape == "hexagon": #if user inputs hexagon
    Hexagon(shapecolor, size)

if shape == "circle": #if user inputs circle
    Circle(shapecolor, size)

if shape == "star": #if user inputs star
    Star(shapecolor, size)

if shape == "triangle": #if user inputs triangle
    Triangle(shapecolor, size)



